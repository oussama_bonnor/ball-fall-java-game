package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Oussama on 25/09/2016.
 */

public class pan extends JPanel {
    int x = 220;
    int score;
    boolean loss;
    boolean finished;
    int speed = 0;
    int bestscore = 0;
    boolean pause;
    ArrayList<cercle> c = new ArrayList<>();
    boolean playAgain;

    public void go() {

        score = 0;
        loss = false;
        finished = false;
        pause = false;
        playAgain = false;
        c.removeAll(c);
        fill();
        repaint();
        while (true) {
            if (!loss && !pause) {
                for (int i = 0; i < c.size(); i++) {
                    c.get(i).y -= 2 + (score / 10);
                }
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                x += speed;
                //speed control "slide"
                if (speed > 0) speed--;
                if (speed < 0) speed++;

                //adding cercles
                if (c.get(0).y <= 50 && c.get(0).y > 48) {
                    fill();
                }
                if (c.get(0).y <= 200 && c.get(0).y > 198) {
                    fill();
                }
                if (c.get(0).y <= 200 && c.get(0).y > 196 && score >= 30 && score <= 40) {
                    fill();
                }

                //deleting cercles
                if (c.get(0).y <= -40 & c.size() > 0) {
                    delete();
                }

                //in case of emergencie
                if (c.size() == 0) {
                    fill();
                }

                //moving balls position in case of getting out of window
                if (x > 460) {
                    x = -10;
                }
                if (x < -10) {
                    x = 459;
                }

                //scoring...well shit
                if (x >= c.get(0).x && x <= c.get(0).x + (c.get(0).width - 30) && !c.get(0).passed) {
                    if (c.get(0).y <= 55) {
                        score++;
                        c.get(0).passed = true;
                    }
                }

                //collisions (loss)
                if (c.get(0).x >= x && c.get(0).x <= x + 30 && !c.get(0).passed || c.get(0).x + c.get(0).width >= x && c.get(0).x + c.get(0).width - 30 <= x && !c.get(0).passed) {
                    if (c.get(0).y <= 60 && c.get(0).y >= 35) {
                        loss = true;
                    }
                }

                repaint();
            }

            if (loss && !pause) {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finished = true;
                if (score > bestscore) bestscore = score;
                repaint();

                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (playAgain) go();
                else System.exit(0);
            }
        }
    }

    public void fill() {
        Random r = new Random();
        int k = r.nextInt(15);
        cercle test = new cercle((k * 20) + 20, 700, 100 - score, 40, false);
        c.add(test);
    }

    public void delete() {
        c.remove(0);
    }

    public void paintComponent(Graphics g) {
        if (!finished) {
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLUE);
            g.fillOval(x, 50, 30, 30);
            if (score >= 0 && score <= 10) {
                g.setColor(Color.WHITE);
            }
            if (score > 10 && score <= 20) {
                g.setColor(Color.red);
            }
            if (score > 20 && score <= 30) {
                g.setColor(Color.green);
            }
            if (score > 30) {
                g.setColor(Color.ORANGE);
            }

            for (int i = 0; i < c.size(); i++) {
                g.drawArc(c.get(i).x, c.get(i).y, c.get(i).width, c.get(i).hight, 360, 360);
            }
            g.setColor(Color.WHITE);
            g.drawString("score: " + score, 20, 50);
            g.drawString("Best score: " + bestscore, 380, 50);
            g.drawString("to Pause: Space", 20, 30);
            g.drawString("to Quit: Escape", 380, 30);
            if (pause) {
                g.drawString("PAUSE", getWidth() / 2 - 10, getHeight() / 2);
            }
        } else {
            g.setColor(Color.white);
            g.fillRect(0, 0, getWidth(), getHeight());
            g.setColor(Color.BLACK);
            g.drawString("YOU LOST!", getWidth() / 2 - 20, getHeight() / 2);
            g.drawString("Score: " + score, getWidth() / 2 - 12, getHeight() / 2 + 15);
            g.drawString("Best score: " + bestscore, getWidth() / 2 - 15, getHeight() / 2 + 30);
            g.drawString("To play again press Enter.", getWidth() / 2 - 50, getHeight() / 2 + 45);
        }
    }
}