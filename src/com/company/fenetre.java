package com.company;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * Created by Oussama on 25/09/2016.
 */
public class fenetre extends JFrame implements KeyListener {
    pan pan = new pan();

    public fenetre() {
        this.setTitle("Ball Fall");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(480, 720);
        this.setContentPane(pan);
        this.setVisible(true);
        this.setResizable(false);
        addKeyListener(this);
        pan.repaint();
        pan.go();
    }

  /*  public void go() {
        pan.fill();
        pan.repaint();
        do {
            for (int i = 0; i < pan.c.size(); i++) {
                pan.c.get(i).y -= 2 + (pan.score/10);
            }
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            pan.x += speed;
            //speed control "slide"
            if (speed > 0) speed--;
            if (speed < 0) speed++;

            //adding cercles
            if (pan.c.get(0).y <= 50 && pan.c.get(0).y > 48) {
                pan.fill();
            }
            if (pan.c.get(0).y <= 200 && pan.c.get(0).y > 198) {
                pan.fill();
            }

            //deleting cercles
            if (pan.c.get(0).y <= -40) {
                pan.delete();
            }

            //moving balls position in case of getting out of window
            if (pan.x > 460) {
                pan.x = -10;
            }
            if (pan.x < -10) {
                pan.x = 459;
            }

            //scoring...well shit
            if (pan.x >= pan.c.get(0).x && pan.x <= pan.c.get(0).x + 70 && !pan.c.get(0).passed) {
                if (pan.c.get(0).y <= 55) {
                    pan.score++;
                    pan.c.get(0).passed = true;
                }
            }

            //collisions (loss)
            if (pan.c.get(0).x >= pan.x && pan.c.get(0).x <= pan.x + 30 && !pan.c.get(0).passed ||  pan.c.get(0).x + 100 >= pan.x && pan.c.get(0).x + 70 <= pan.x && !pan.c.get(0).passed) {
                if (pan.c.get(0).y <= 60) {
                    pan.loss = true;
                }
            }

            pan.repaint();
        }
        while (!pan.loss);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        pan.finished = true;
        pan.repaint();
    }
*/

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        int k = e.getKeyCode();
        if (k == KeyEvent.VK_RIGHT) {
            if (pan.speed < 8) {
                pan.speed += 2;
                pan.repaint();
            }
        }
        if (k == KeyEvent.VK_LEFT) {
            if (pan.speed > -8) {
                pan.speed -= 2;
                pan.repaint();
            }
        }
        if (k == KeyEvent.VK_SPACE) {
            pan.pause = !pan.pause;
        }

        if (k == KeyEvent.VK_ENTER) {
            if (!pan.playAgain) {
                pan.playAgain = true;
            }
        }
        if (k == KeyEvent.VK_ESCAPE) {
            System.exit(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}
