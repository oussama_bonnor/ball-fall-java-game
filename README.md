# Ball-fall
Small game made using Java Swing, simple and understandable through the first playing session.
Just keep the ball inside the cercles for points, avoid hitting the sides of the circles, enjoy!

![image](https://cloud.githubusercontent.com/assets/17766221/22128611/d556cb40-dea1-11e6-8017-0d34feb311eb.png)

![image](https://cloud.githubusercontent.com/assets/17766221/22128628/e8f3727a-dea1-11e6-9984-e5e69ae053fb.png)

![image](https://cloud.githubusercontent.com/assets/17766221/22128646/fccbbffa-dea1-11e6-8a29-cc0243629c85.png)
